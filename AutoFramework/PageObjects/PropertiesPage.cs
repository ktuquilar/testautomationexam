﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace AutomationTest.PageObjects
{
    public class PropertiesPage
    {
        public PropertiesPage(IWebDriver Driver2)
        {
            PageFactory.InitElements(Driver2, this);
        }

        [FindsBy(How = How.ClassName, Using = "entry-title")]
        public IWebElement PageTitle { get; set; }

        [FindsBy(How = How.ClassName, Using = "sort-sel")]
        public IWebElement DrpSortBy { get; set; }

        [FindsBy(How = How.CssSelector, Using = "#content-listings > ul.list")]
        public IWebElement PropertiesList { get; set; }


        // ---------- Elements concatenation paths ---------- //
        public string PropertyPrice = "div.prop-det > p.list-price > span";
        public string PropertyTitle = "div.prop-det > span > a";
        public string PropertyImage = "div.prop-img-wrap > a > img.main-img.wp-post-image";
    }
}
