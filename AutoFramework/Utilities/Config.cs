﻿namespace AutomationTest
{
    public static class Config
    {
        public static int ElementsWaitingTimeout = 5;
        public static string baseURL = "https://qaexam.forge99.com/properties";

        //PROPERTIES PAGE
        public static class SortOptions
        {
            public static string PriceDescending = "Price Descending";
            public static string PriceAscending = "Price Ascending";
            public static string MostRecent = "Most Recent";
            public static string AZ = "A-Z";
            public static string ZA = "Z-A";
        }


        public static class PropertyImage
        {
            public static int ExpectedClientWidth = 300;
        }
    }
}

