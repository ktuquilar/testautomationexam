﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using System;

namespace AutomationTest
{
    public static class Actions
    {
        public static IWebDriver InitializeDriver()
        {
            IWebDriver Driver2 = new ChromeDriver();
            Driver2.Manage().Window.Maximize();
            Driver2.Navigate().GoToUrl(Config.baseURL);

            TimeSpan seconds = TimeSpan.FromSeconds(Config.ElementsWaitingTimeout);
            Driver2.Manage().Timeouts().ImplicitWait = seconds;

            return Driver2;
        }
    }
}
