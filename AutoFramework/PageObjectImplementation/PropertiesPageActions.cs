﻿using AutomationTest.PageObjects;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;


namespace AutoFramework.PageObjectImplementation
{

    public class PropertiesPageActions
    {
        // ---------- Clicks Sort By option ----------  //
        public static void SortListBy(string sortOption, IWebDriver Driver2)
        {
            PropertiesPage _PropertiesPage = new PropertiesPage(Driver2);

            SelectElement drpSortBy = new SelectElement(_PropertiesPage.DrpSortBy);

            switch (sortOption)
            {
                case "Price Descending":
                    drpSortBy.SelectByText("Price Descending");
                    break;
                case "Price Ascending":
                    drpSortBy.SelectByText("Price Ascending");
                    break;
                case "A-Z":
                    drpSortBy.SelectByText("A-Z");
                    break;
                case "Z-A":
                    drpSortBy.SelectByText("Z-A");
                    break;
            }
            Thread.Sleep(3000);
        }


        // ---------- Checks default list order by Price, then create a list for them ----------  //
        public static List<int> VerifyInitialListByPrice(IWebDriver Driver2)
        {
            PropertiesPage _PropertiesPage = new PropertiesPage(Driver2);

            List<int> initialList = new List<int>();
            IWebElement propertiesList = _PropertiesPage.PropertiesList;
            IReadOnlyList<IWebElement> listItems = propertiesList.FindElements(By.CssSelector("li"));

            foreach (var item in listItems)
            {
                var propertyPrice = item.FindElement(By.CssSelector(_PropertiesPage.PropertyPrice));
                initialList.Add(int.Parse(propertyPrice.Text.Replace("$", ""), NumberStyles.AllowThousands));
            }
            return initialList;
        }


        // ---------- Checks default list order by Title/Address, then create a list for them ----------  //
        public static List<string> VerifyInitialListByTitle(IWebDriver Driver2)
        {
            PropertiesPage _PropertiesPage = new PropertiesPage(Driver2);

            List<string> initialList = new List<string>();
            IWebElement propertiesList = _PropertiesPage.PropertiesList;
            IReadOnlyList<IWebElement> listItems = propertiesList.FindElements(By.CssSelector("li"));

            foreach (var item in listItems)
            {
                var propertyTitle = item.FindElement(By.CssSelector(_PropertiesPage.PropertyTitle));
                initialList.Add(propertyTitle.Text);
            }
            return initialList;
        }


        // ---------- Verify sorted list by Price by comparing expected and actual list----------  //
        public static void VerifySortedListByPrice(List<int> initialList, string order, IWebDriver Driver2)
        {
            PropertiesPage _PropertiesPage = new PropertiesPage(Driver2);

            List<int> expectedList = PropertiesPageActions.CreateExpectedListByPrice(initialList, order);

            List<int> sortedList = new List<int>();
            IWebElement propertiesList = _PropertiesPage.PropertiesList;
            IReadOnlyList<IWebElement> listItems = propertiesList.FindElements(By.CssSelector("li"));

            switch (order)
            {
                case "Price Descending":
                    foreach (var item in listItems)
                    {
                        var propertyPrice = item.FindElement(By.CssSelector(_PropertiesPage.PropertyPrice));
                        sortedList.Add(int.Parse(propertyPrice.Text.Replace("$", ""), NumberStyles.AllowThousands));
                    }
                    break;
                case "Price Ascending":
                    foreach (var item in listItems)
                    {
                        var propertyPrice = item.FindElement(By.CssSelector(_PropertiesPage.PropertyPrice));
                        sortedList.Add(int.Parse(propertyPrice.Text.Replace("$", ""), NumberStyles.AllowThousands));
                    }
                    break;
            }
            CollectionAssert.AreEqual(expectedList, sortedList, "INCORRECT SORTING.");
        }


        // ---------- Verify sorted list by Title by comparing expected and actual list----------  //
        public static void VerifySortedListByTitle(List<string> initialList, string order, IWebDriver Driver2)
        {
            PropertiesPage _PropertiesPage = new PropertiesPage(Driver2);

            List<string> expectedList = PropertiesPageActions.CreateExpectedListByTitle(initialList, order);

            List<string> sortedList = new List<string>();
            IWebElement propertiesList = _PropertiesPage.PropertiesList;
            IReadOnlyList<IWebElement> listItems = propertiesList.FindElements(By.CssSelector("li"));

            switch (order)
            {
                case "A-Z":
                    foreach (var item in listItems)
                    {
                        var propertyTitle = item.FindElement(By.CssSelector(_PropertiesPage.PropertyTitle));
                        sortedList.Add(propertyTitle.Text);
                    }
                    break;
                case "Z-A":
                    foreach (var item in listItems)
                    {
                        var propertyTitle = item.FindElement(By.CssSelector(_PropertiesPage.PropertyTitle));
                        sortedList.Add(propertyTitle.Text);
                    }
                    break;
            }
            CollectionAssert.AreEqual(expectedList, sortedList, "INCORRECT SORTING.");
        }


        // ---------- Creates expected order of list by Price ----------  //
        public static List<int> CreateExpectedListByPrice(List<int> initialList, string order)
        {
            List<int> expectedList = new List<int>();

            switch (order)
            {
                case "Price Descending":
                    expectedList = initialList.OrderByDescending(i => i).ToList();
                    break;
                case "Price Ascending":
                    expectedList = initialList.OrderByDescending(i => i).ToList();
                    expectedList.Reverse();
                    break;
            }
            return expectedList;
        }


        // ---------- Creates expected order of list by Title ----------  //
        public static List<string> CreateExpectedListByTitle(List<string> initialList, string order)
        {
            List<string> expectedList = new List<string>();

            switch (order)
            {
                case "A-Z":
                    expectedList = initialList.OrderByDescending(i => i).ToList();
                    expectedList.Reverse();
                    break;
                case "Z-A":
                    expectedList = initialList.OrderByDescending(i => i).ToList();
                    break;
            }
            return expectedList;
        }


        // ---------- Checks Property image client width ----------  //
        public static void VerifyPropertyImageClientWidth(int expectedClientWidth, IWebDriver Driver2)
        {
            PropertiesPage _PropertiesPage = new PropertiesPage(Driver2);

            IWebElement propertiesList = _PropertiesPage.PropertiesList;
            IReadOnlyList<IWebElement> listItems = propertiesList.FindElements(By.CssSelector("li"));
            
            int x = 0;
            int i = 0;
            foreach (var item in listItems)
            {
                var propertyImage = item.FindElement(By.CssSelector(_PropertiesPage.PropertyImage));
                var clientWidth = int.Parse(propertyImage.GetAttribute("clientWidth"));

                if (clientWidth != expectedClientWidth)
                {
                    Console.WriteLine("Property " + (x + 1) + " clientWidth was " + clientWidth);
                    i = 1;
                }
                x++;
            }

            if (i > 0)
            {
                Assert.Fail("Property image's clientWidth should be " + expectedClientWidth + "px.");
            }
        }
    }
}

