﻿using AutoFramework.PageObjectImplementation;
using AutomationTest;
using NUnit.Framework;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutoFramework.Test.PropertiesPage
{
    [Parallelizable]
    public class CheckPropertyImage
    {
        IWebDriver Driver2;

        [OneTimeSetUp]
        public void Initialize()
        {
            Driver2 = Actions.InitializeDriver();
        }


        [Test]
        public void ClientWidth()
        {
            int ExpectedClientWidth = Config.PropertyImage.ExpectedClientWidth;

            PropertiesPageActions.VerifyPropertyImageClientWidth(ExpectedClientWidth, Driver2);
        }


        [OneTimeTearDown]
        public void CleanUp()
        {
            Driver2.Quit();
        }
    }
}
