﻿using OpenQA.Selenium;
using NUnit.Framework;
using AutoFramework.PageObjectImplementation;
using System.Collections.Generic;
using System;
using AutomationTest.PageObjects;
using System.Globalization;
using OpenQA.Selenium.Support.UI;

namespace AutomationTest
{

    [Parallelizable]
    public class SortProperties
    {
        IWebDriver Driver2;

        [OneTimeSetUp]
        public void Initialize()
        {
            Driver2 = Actions.InitializeDriver();
        }


        [Test]
        public void ByPriceDescending()
        {
            string descending = Config.SortOptions.PriceDescending;

            List<int> initialList = PropertiesPageActions.VerifyInitialListByPrice(Driver2);
            PropertiesPageActions.SortListBy(descending, Driver2);
            PropertiesPageActions.VerifySortedListByPrice(initialList, descending, Driver2);
        }


        [Test]
        public void ByPriceAscending()
        {
            string ascending = Config.SortOptions.PriceAscending;

            List<int> initialList = PropertiesPageActions.VerifyInitialListByPrice(Driver2);
            PropertiesPageActions.SortListBy(ascending, Driver2);
            PropertiesPageActions.VerifySortedListByPrice(initialList, ascending, Driver2);
        }


        [Test]
        public void ByTitleAtoZ()
        {
            string AtoZ = Config.SortOptions.AZ;

            List<string> initialList = PropertiesPageActions.VerifyInitialListByTitle(Driver2);
            PropertiesPageActions.SortListBy(AtoZ, Driver2);
            PropertiesPageActions.VerifySortedListByTitle(initialList, AtoZ, Driver2);
        }


        [Test]
        public void ByTitleZtoA()
        {
            string ZtoA = Config.SortOptions.ZA;

            List<string> initialList = PropertiesPageActions.VerifyInitialListByTitle(Driver2);
            PropertiesPageActions.SortListBy(ZtoA, Driver2);
            PropertiesPageActions.VerifySortedListByTitle(initialList, ZtoA, Driver2);
        }

        [OneTimeTearDown]
        public void CleanUp()
        {
            Driver2.Quit();
        }
    }
}