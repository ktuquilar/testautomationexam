# TestAutomationExam

This project was made for August99 test automation exam.

### Setup / Checklist:

1. Open the project folder "testautomationexam" on your local.
2. Open the "AutomationTest.sln" on Visual Studio.
3. Check that the following packages below are installed:

    - DotNetSeleniumExtras.PageObjects
    - NUnit
    - NUnit3TestAdapter
    - Selenium.Support
    - Selenium.WebDriver
    - Selenium.WebDriver.ChromeDriver
    
> To check packages, go to Tools -> NuGet Package Manager -> Manage NuGet Packages for Solution ("NuGet - Solution" tab will be opened).<br />
> Then, click "Installed" tab to verify packages (skip step 4 if all packages are installed).<br />
    
4. To install missing package(s):
    - Click "Browse" tab and search package name.
    - Select the package. Then on the right side of the window, tick the solution name "AutomationTest" ("Install" button will be enabled).
    - Click on the "Install" button.
    
5. Click "Build" -> "Rebuild Solution".

### To display test items:

1. Go to Test -> Test Explorer.
2. Click on the Total Test icon (erlenmeyer flask) to display all test items.

### To run test(s):
1. To run all tests, click on the "Run All Tests In View" icon on Test Explorer.
2. To run specific test(s), select test item(s), then click "Run" icon.

